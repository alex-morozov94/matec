// comments
$(function () {
	$('.accordion-steps').livequery(function () {
		var $context = $(this);

		// Фиксирование блока с результатом. В скриптах компонента

		var $step = $('.accordion-step', $context);

		// Изменить данные шага
		var $stepSettings = $('.accordion-step__editstep', $step);
		$stepSettings.on('click', function () {
			$(this).parents('.accordion-step').removeClass('bx-step-completed');
			return false;
		});

		// Переход к следующему шагу
		var $nextLink = $('.accordion-step__next', $step);
		$nextLink.on('click', function () {
			var $currentStep = $(this).parents('.accordion-step');
			$currentStep.addClass('bx-step-completed');
			var $nextStep = $currentStep.next();
			$nextStep.addClass('bx-active');
			$nextStep.remove('bx-step-completed');
			return false;
		});

		// Переход к предыдущему шагу
		var $prevLink = $('.accordion-step__prev', $step);
		$prevLink.on('click', function () {
			var $currentStep = $(this).parents('.accordion-step');
			$currentStep.addClass('bx-step-completed');
			var $prevStep = $currentStep.prev();
			$prevStep.addClass('bx-active');
			$prevStep.remove('bx-step-completed');
			return false;
		});

		var $customCheckbox = $('.accordion-step__item-container .accordion-step__pp-company-graf-container', $context);
		$customCheckbox.on('click', function() {
			var $variantsWrapper = $(this).parents('.accordion-step__item-container');
			$variantsWrapper.find('.accordion-step__pp-company').removeClass('bx-selected');
			var $variant = $(this).parents('.accordion-step__pp-company');
			$variant.addClass('bx-selected');
			var $id = $variant.data('id');
			var $resultWrapper = $variantsWrapper.next('.accordion-step__pp-desc-container');

			// Чекбоксы
			$variantsWrapper.find('input[type=checkbox]').removeAttr("checked");
			$variant.find('input[type=checkbox]').prop("checked", true);

			// Результат выбора
			$resultWrapper.find('.accordion-step__pp-company').addClass('is-hidden');
			$resultWrapper.find('.accordion-step__pp-company[data-id="'+$id+'"]').removeClass('is-hidden');
		});
	})
});

$(function(){
	/* $('.auth-form').each(function(){
		var $block = $(this);
		$block.after('<div class="js-dummy-block"><a href="#" class="js-show-in-popup">Показать в попапе</a></div>');
		$block.next().find('.js-show-in-popup').on('click', function(e){
			e.preventDefault();
			$.fancybox($block, {padding: 0});
		});
	}); */
});

// Плитка товара в каталоге
	$(function () {
		$('.catalog-plate').each(function () {
			var $context = $(this);
			var $favor = $('.catalog-plate__favor', $context);

			function toggleFavor () {
				var $link = $(this);

				$link.toggleClass('is-active');
				$.get($link.attr('href'));
				return false;
			}

			$favor.on('click', toggleFavor);

		});
	});

// comments
$(function () {
    $('.compare-result').livequery(function () {
        var $context = $(this);
        var $table = $('.compare-result__table', $context);
        var $removeLinks = $('.compare-result__remove', $context);
        var $products = $('.compare-result__product', $context);
        var $emptyMessage = $('.compare-result__empty', $context);
        var $showAll = $('.compare-result__show-all', $context);
        var $showDiff = $('.compare-result__show-different', $context);
        var $header = $('.compare-result__header', $context);
        var collapsed = false;

        (function init() {
            $context.removeClass('no-js');
            initTableAdaptive();
            initRemoveHandler();
            initViewModeChange();
        })();

        function initRemoveHandler() {
            $removeLinks.on('click', function (e) {
                var $link = $(this);
                var $table = $link.closest('table');
                var rowIndex = $link.closest('td').index();

                $('tr', $table).each(function () {
                    var $row = $(this);
                    var $cells = $('td', $row);

                    $cells.eq(rowIndex).remove();
                });

                updateTables();
                e.preventDefault();
            });
        }

        function initTableAdaptive() {
            //- adaptive based on http://zurb.com/playground/responsive-tables
            var tableWidth;

            if($table.length == 0) return;

            $(window).load(updateTables);
            $(window).on("resize", updateTables);
        }

        function initViewModeChange () {
            $showAll.on('click', function (e) {
                $('.compare-result__table', $context).removeClass('is-diff');
                $showAll.addClass('is-active');
                $showDiff.removeClass('is-active');
                $('.compare-result__param-row', $context).removeClass('is-odd');
                e.preventDefault();
            });

            $showDiff.on('click', function (e) {
                $('.compare-result__table', $context).addClass('is-diff');
                $showAll.removeClass('is-active');
                $showDiff.addClass('is-active');
                $('.compare-result__table', $context).each(function () {
                    $('.compare-result__param-row.is-different:odd', $(this)).addClass('is-odd');
                });
                e.preventDefault();
            });
        }

        function updateTables() {
            if ($table.width() > $context.width() && !collapsed) {
                collapsed = true;
                $table.each(function() {splitTable($(this));});
                return true;
            } else if (collapsed &&
                $table.width() <= $('.compare-result__table-scrollable', $context).width())
            {
                collapsed = false;
                $table.each(function() {unsplitTable($(this));});
            }

            if($table.find($products).length == 0) showEmptyTable();
        };

        function splitTable(original) {
            original.wrap("<div class='compare-result__table-wrapper' />");

            var copy = original.clone();
            copy.find("td:not(:first-child), th:not(:first-child)").css("display", "none");

            original.closest(".compare-result__table-wrapper").append(copy);
            copy.wrap("<div class='compare-result__table-pinned' />");
            original.wrap("<div class='compare-result__table-scrollable'/>");

            setCellHeights(original, copy);
        }

        function unsplitTable(original) {
            original.closest(".compare-result__table-wrapper").find(".compare-result__table-pinned").remove();
            original.unwrap();
            original.unwrap();
        }

        function showEmptyTable($table) {
            $('.compare-result__table-holder', $context).remove();
            $emptyMessage.addClass('is-active');
            $header.remove();
            $('html, body').css({scrollTop: $context.offset().top - 200});
        }

        function setCellHeights(original, copy) {
            var tr = original.find('tr'),
                tr_copy = copy.find('tr'),
                heights = [];

            tr.each(function (index) {
                var self = $(this),
                    tx = self.find('th, td');

                tx.each(function () {
                    var height = $(this).outerHeight(true);
                    heights[index] = heights[index] || 0;
                    if (height > heights[index]) heights[index] = height;
                });

            });

            tr_copy.each(function (index) {
                $(this).height(heights[index]);
            });
        }
    });
});

$(function(){
	$('.forgotpasswd-form').each(function(){
		var $context = $(this);
		var $successMessage = $('.success-message-modal', $context);
		$context.after('<div class="js-dummy-block"><a href="#" class="js-show-in-popup">Показать в попапе</a> <a href="#" class="js-show-success-message">Успешно</a></div>');
		$context.next().find('.js-show-in-popup').on('click', function(e){
			e.preventDefault();
			$.fancybox($context, {padding: 0});
		});
		$context.next().find('.js-show-success-message').on('click', function(e){
			e.preventDefault();
			$.fancybox($successMessage, {padding: 0});
		});
	});
});

var isLocalBuild = true;


// Отлавливаем событие построения результата
$(document).bind('constructor_calculate_result', function (e, selectedVariants, allSelected, callback) {
	// Обработка id вариантов и возврат наименований
	console.log(selectedVariants);
	console.log(allSelected);
	var selectedVariantsResult = [];
	jQuery.each(selectedVariants, function(i, val) {
		selectedVariantsResult[i] = val;
	});
	callback({
		price: 4000,
		variants: selectedVariantsResult
	});
});

// Отлавливаем событие построения вариантов
$(document).bind('constructor_calculate_variants', function (e, selectedVariants, selectedVariantsTmp, nextProp, currentProp, resultVisible, callback) {
	// Обработка id вариантов и возврат наименований
	var availableVariants;
	// avalibaleVariants = { ID_СВОИСТВА: МАССИВ ДОСТУПНЫХ ВАРИАНТОВ];
	availableVariants = {
		1: ["21", "21.5", "22", "22.5", "23", "23.5"],
		2: ["Черный сатин (креп-сатин)", "Белый сатин"],
		3: ["8 см", "9 см"],
		4: ["e"],
		5: ["Нормальная", "Узкая"],
		6: ["К (Карина, норма)"]
	};
	callback({
		availableVariants: availableVariants,
		selectedVariantsTmp: selectedVariantsTmp,
		nextProp: nextProp,
		currentProp: currentProp,
		resultVisible: resultVisible
	},nextProp);
});

$(function () {
	$('.product-detail').livequery(function () {
		var $context = $(this);
		$context.catalogPlate({
			selectsProperty: ["COLOR_REF", "SIZES_CLOTHES"],
			propertyView:{
				"ARTICLE": "js-article"
			},
			offers : {
				"608": {
					"ID": 608,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=608",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=608",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "red",
						"SIZES_CLOTHES": "46"
					},
					"RATIO_PRICE": {
						"VALUE": 2000,
						"DISCOUNT_VALUE": 2000,
						"PRINT_VALUE": "<span>2 000<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>2 000<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					},
				},
				"607": {
					"ID": 607,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "green",
						"SIZES_CLOTHES": "48"
					},
					"RATIO_PRICE": {
						"VALUE": 1000,
						"DISCOUNT_VALUE": 1000,
						"PRINT_VALUE": "<span>1 000<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 000<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"606": {
					"ID": 606,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "yellow",
						"SIZES_CLOTHES": "50"
					},
					"RATIO_PRICE": {
						"VALUE": 1333,
						"DISCOUNT_VALUE": 1333,
						"PRINT_VALUE": "<span>1 333<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 333<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"605": {
					"ID": 605,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "yellow",
						"SIZES_CLOTHES": "52"
					},
					"RATIO_PRICE": {
						"VALUE": 1222,
						"DISCOUNT_VALUE": 1222,
						"PRINT_VALUE": "<span>1 222<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 222<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"604": {
					"ID": 604,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "yellow",
						"SIZES_CLOTHES": "54"
					},
					"RATIO_PRICE": {
						"VALUE": 1111,
						"DISCOUNT_VALUE": 1111,
						"PRINT_VALUE": "<span>1 111<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 111<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				}
			}
		});

	})
});
$(function () {
	$('.product-detail-modal').livequery(function () {
		var $context = $(this);
		$context.catalogPlate({
			selectsProperty: ["COLOR_REF", "SIZES_CLOTHES"],
			propertyView:{
				"ARTICLE": "js-article"
			},
			offers : {
				"608": {
					"ID": 608,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=608",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=608",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "red",
						"SIZES_CLOTHES": "46"
					},
					"RATIO_PRICE": {
						"VALUE": 2000,
						"DISCOUNT_VALUE": 2000,
						"PRINT_VALUE": "<span>2 000<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>2 000<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"607": {
					"ID": 607,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "green",
						"SIZES_CLOTHES": "48"
					},
					"RATIO_PRICE": {
						"VALUE": 1000,
						"DISCOUNT_VALUE": 1000,
						"PRINT_VALUE": "<span>1 000<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 000<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"606": {
					"ID": 606,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "yellow",
						"SIZES_CLOTHES": "50"
					},
					"RATIO_PRICE": {
						"VALUE": 1333,
						"DISCOUNT_VALUE": 1333,
						"PRINT_VALUE": "<span>1 333<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 333<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"605": {
					"ID": 605,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "yellow",
						"SIZES_CLOTHES": "52"
					},
					"RATIO_PRICE": {
						"VALUE": 1222,
						"DISCOUNT_VALUE": 1222,
						"PRINT_VALUE": "<span>1 222<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 222<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				},
				"604": {
					"ID": 604,
					"CATALOG_WEIGHT": "0",
					"BUY_URL": "?region=msk&amp;action=BUY&amp;id=607",
					"ADD_URL": "?region=msk&amp;action=ADD2BASKET&amp;id=607",
					"COMPARE_URL": null,
					"PROPERTIES": {
						"COLOR_REF": "yellow",
						"SIZES_CLOTHES": "54"
					},
					"RATIO_PRICE": {
						"VALUE": 1111,
						"DISCOUNT_VALUE": 1111,
						"PRINT_VALUE": "<span>1 111<\/span> \u0440\u0443\u0431.",
						"PRINT_DISCOUNT_VALUE": "<span>1 111<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF": 0,
						"PRINT_DISCOUNT_DIFF": "<span>0<\/span> \u0440\u0443\u0431.",
						"DISCOUNT_DIFF_PERCENT": 0,
						"CURRENCY": "RUB"
					}
				}
			}
		});

	})
});
$(function(){
	$('.registration-form.is-modal, .registration-form.is-email-request').each(function(){
		var $context = $(this);
		var $successMessage = $('.success-message-modal', $context);
		$context.after('<div class="js-dummy-block"><a href="#" class="js-show-in-popup">Показать в попапе</a> <a href="#" class="js-show-success-message">Успешная регистрация</a></div>');
		$context.next().find('.js-show-in-popup').on('click', function(e){
			e.preventDefault();
			$.fancybox($context, {padding: 0});
		});
		$context.next().find('.js-show-success-message').on('click', function(e){
			e.preventDefault();
			$.fancybox($successMessage, {padding: 0});
		});
	});
});

// Строка поиска
	$(function () {
		$('.search-title').livequery(function () {
			var $context = $(this);
			var $input = $('.search-title__input', $context);
			var $resultsHolder = $('.search-title__results-holder', $context);
			$resultsHolder.css({display:'none'});
			var isNotInteractive = $context.is('.is-not-interactive');


			function showResultLightbox(event) {
				var val = $(this).val();
				console.log('showResultLightbox');

				if(val !== '') {
					$resultsHolder.css({display:'block'});
				} else {
					$resultsHolder.css({display:'none'});
				}
			}

			if(! isNotInteractive){
				$input.on('keyup', showResultLightbox);
			}
		});
	});

$(function () {
	$('.smart-tiles').livequery(function () {
		var $context = $(this);

		var $tilesFix = $('.smart-tiles__fix', $context);
		$tilesFix.addClass('is-inited');
	});
});
// Тест перестраивания слайдеров при ресайзе
$(window).resize(function() {
	initPromoSliders(true);
});
// Умный фильтр
	$(function () {
		$('.smart-filter-dropdown').livequery(function () {
			var $context = $(this);
			var $fields = $('.smart-filter-dropdown__field', $context);
			var $main = $('.smart-filter-dropdown__main', $context);
			var $fieldCaptions = $('.smart-filter-dropdown__field-header', $context);
			var $contentHolders = $('.smart-filter-dropdown__field-content', $context);
			var $fieldResets = $('.smart-filter-dropdown__field-reset', $context);
			var $fullReset = $('.smart-filter-dropdown__full-reset', $context);
			var $fieldApply = $('.smart-filter-dropdown__field-btn', $context);
			var $allTags = $('.smart-filter-dropdown__tag', $context);
			var $activeField = $();

			(function init() {
				initTags();

				$allTags.on('click', function () {
					$(this).remove();
					$context.trigger('resize.block');
				});

			})();

			function initTags () {
				$fieldApply.each(function () {
					var $applyLink = $(this);
					var $field = $applyLink.closest($fields);
					var $tags = $('.smart-filter-dropdown__tags-holder', $field);

					$applyLink.on('click', function (e) {
						var tags = [];
						$('[data-tag]:checked, [data-tag].is-tagged', $field).each(function () {
							tags.push({val: $(this).data('tag').replace('{val}', $(this).val()), link: $(this)});
						});

						$tags.find('.smart-filter-dropdown__tag').remove();

						$.each(tags, function (key, tag) {
							var $newTag = $('<span class="smart-filter-dropdown__tag">'+tag.val+'</span>');
							$tags.append($newTag);

							$newTag.on('click', function () {
								$(this).remove();
								$context.trigger('resize.block');
							});

							$context.trigger('resize.block');
						});

						$field.removeClass('is-open')
						e.preventDefault();
					});
				});
			}

		});
	});

// Отлавливаем событие проставления оценки
$(document).bind('vote_stars_set_value', function (e, value ) {
	console.warn('DUMMYrating_form_set_value',value);
});
