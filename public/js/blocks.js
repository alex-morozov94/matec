
$(function(){
	$('.header-mobile').livequery(function(){
		var $context = $(this);
		var $buttons = $('a[data-action="mobile-menu"]', $context);
		var isMenuOpened = false;

		function closeActiveMenus(isSwitch){
			$buttons = $('a[data-action="mobile-menu"]', $context); // dom changes, so buttons must be set again
			$buttons.filter('.is-active').each(function(){
				$(this).removeClass('is-active');
				$($(this).attr('href')).removeClass('is-active');
			});
			if(!isSwitch){
				$context.removeClass('is-menu-opened');
				isMenuOpened = false;
				window.endPreventBodyScroll();
			}	
		}
		$(document).on('click', 'a[data-action="mobile-menu"]', function(e){
			var $button = $(this);
			var $plate = $($button.attr('href'));
			e.preventDefault();
			if($button.is('.is-active')){
				closeActiveMenus();
			} else {
				closeActiveMenus(true);
				$button.addClass('is-active');
				$plate.addClass('is-active');

				if($plate.height() >= $(window).height()*0.8) {
					$plate.addClass('is-fullscreen');
				} else {
					$plate.removeClass('is-fullscreen');
				}
				
				$context.addClass('is-menu-opened');
				isMenuOpened = true;

				window.startPreventBodyScroll();
			}
		});
		$(document).on('touchstart', function(e){
			if(isMenuOpened && $(e.target).closest('.header-mobile').length === 0){
				closeActiveMenus();
			}
		});
		$(document).on('cartDataAvailable', function(e){
			if(isMenuOpened){
				$('.header-mobile__cart-button, .header-mobile__cart-holder').addClass('is-active');
			}
		});
		$(document).on('fancybox-before-show', function(){
			closeActiveMenus();
		});
	});
});
// Древовидное меню
	$(function () {
		// Инициализация только для состояния И
		$('.menu-top').livequery(function () {
			var $context = $(this);
			var $firstHolders = $('.menu-top__first-holder', $context);
			var $firstItems = $('.menu-top__first-item', $context);
			var $firstLinks = $('.menu-top__first-link', $context);

			var $secondHolders = $('.menu-top__second-holder', $context);
			var $secondItems = $('.menu-top__second-item', $context);
			var $secondLinks = $('.menu-top__second-link', $context);

			var $thirdHolders = $('.menu-top__third-holder', $context);
			var $thirdItems = $('.menu-top__third-item', $context);
			var $thirdLinks = $('.menu-top__third-link', $context);

			var $fourthHolders = $('.menu-top__fourth-holder', $context);
			var $fourthItems = $('.menu-top__fourth-item', $context);
			var $fourthLinks = $('.menu-top__fourth-link', $context);

			var $allHolders = $firstHolders.add($secondHolders).add($thirdHolders).add($fourthHolders);
			var $allLinks = $firstLinks.add($secondLinks).add($thirdLinks).add($fourthLinks);
			var $allItems = $firstItems.add($secondItems).add($thirdItems).add($fourthItems);

			var $moreLinks = $('.menu-top__more-link', $context);

			function initMoreLinks () {
				$moreLinks.each(function () {
					var $link = $(this);
					var $list = $link.closest('.menu-top__third-level');
					var $items = $list.find('.menu-top__third-item');
					var $less = $list.find('.menu-top__less-link');
					var qtty = $link.data('hide-qtty');

					$less.on('click', function () {
						$items.slice($items.length - (qtty + 1), $items.length - 1).addClass('is-hide');
						$less.removeClass('is-active');
						$link.addClass('is-active');
					});

					$link.on('click', function () {
						$items.removeClass('is-hide');
						$less.addClass('is-active');
						$link.removeClass('is-active');
					});
				});
			}

			initMoreLinks();
		});
	});

// comments
	$(function () {
		$('.block-name').livequery(function () {
			var $context = $(this);

			// code here...
		})
	});

$(function () {
	$(document).trigger('blocksReady');
})
